### The last commit was in June 2018. It probably says enough about this repository. But head up, the new repository with the new version is ready.The activity is higher (it is not difficult to achieve compared to a dead repo), and the repo link at the bottom. Come on, leave a star and forget you did it because you'll probably never use my dot files. Thanks

[New version](https://gitlab.com/xXBlackMaskXx/blackarrow2)

# BlackArrow

Files of my Arch Linux configuration. BlackArrow is based on Dawid Potocki's .dotfiles - [biual](https://gitlab.com/dawidpotocki/biual)

## Installation

Open terminal and type:

```bash
$ cd ~/
$ git clone https://gitlab.com/xXBlackMaskXx/bainstaller
$ cd ~/bainstaller/Files
$ bash BA-Installer.sh
```

## FAQ

> Why "BlackArrow"?

Because I use the nickname "BlackMask" and "Arch" is associated with "Arrow", and "black" and "arrow"... I don't know, ok? I don't know...

> Can I update the configuration files without performing the full installation process?

Yes of course. Just run the script "baupdate.sh", and to do this you need to enter in the terminal:
```bash
$ cd ~/
$ git clone https://gitlab.com/xXBlackMaskXx/bainstaller
$ cd ~/bainstaller/Files
$ bash BA-Update.sh
```

> Does the "BlacArrow" theme support other applications?

Yes, for now only "TelegramDesktop" but we are planning to introduce a telegram theme for android systems. The TelegramDesktop theme is available [here](https://t.me/BATheme)
